#pragma once
#include <cstdio>

struct point
{
	float x;
	float y;
};

float bezier(const point& a, const point& b, const point& c, const point& d, const double t);
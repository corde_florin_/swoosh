// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include <iostream>
#include <string>   // for strings

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write
#include "ZoomTransition.h"
#include "RotateTransition.h"
#include "LumaTransition.h"
#include "Swipe.h"
#include "TimeRemapping.h"

using namespace std;
using namespace cv;

const String sourceVideo1 = "Videos/ReclamaVideo2.mp4";
const String sourceVideo2 = "Videos/ReclamaVideo3.mp4";

VideoCapture openVideo(String videoPath) {
	VideoCapture inputVideo(videoPath);
	if (!inputVideo.isOpened())
	{
		cout << "Could not open the input video: " << videoPath << endl;
	}
	return inputVideo;
}

void testVideoSequence()
{

	VideoCapture video1 = openVideo(sourceVideo1);
	VideoCapture video2 = openVideo(sourceVideo2);
	applySwipeTransition(video1, video2, 0.4, "down", point{ 0,1 }, point{ 0,1 }, point{ 0,1 }, point{ 0,1 });
	cout << "Finished writing" << endl;
}

void MyCallBackFunc(int event, int x, int y, int flags, void* param)
{
	//More examples: http://opencvexamples.blogspot.com/2014/01/detect-mouse-clicks-and-moves-on-image.html
	Mat* src = (Mat*)param;
	if (event == CV_EVENT_LBUTTONDOWN)
	{
		printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
			x, y,
			(int)(*src).at<Vec3b>(y, x)[2],
			(int)(*src).at<Vec3b>(y, x)[1],
			(int)(*src).at<Vec3b>(y, x)[0]);
	}
}

int main()
{
	testVideoSequence();
	return 0;
}
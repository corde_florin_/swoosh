#include "stdafx.h"
#include "common.h"
#include <iostream>
#include <string> 
#include <math.h>
#include "Swipe.h"
#include "Bezier.h"
using namespace std;
using namespace cv;

float computeInRange3(float rangeLeftLimit, float rangeRightLimit, double valueToConvert) {
	//value to convert is in the 0 - 1 range
	return valueToConvert * (rangeRightLimit - rangeLeftLimit) + rangeLeftLimit;
}

Size getVideoSize3(VideoCapture &video) {
	return Size((int)video.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)video.get(CV_CAP_PROP_FRAME_HEIGHT));
}

void moveImage(Mat &originalImage, Mat &movedImage, double percenteges, int direction)
{
	Mat imageWithBorder;
	double pixels = (percenteges / 100) * originalImage.rows;
	if (direction == 1) {
		copyMakeBorder(originalImage, imageWithBorder, pixels, 0, 0, 0, BORDER_REFLECT);
		Rect roi = Rect(0, 0, originalImage.cols, originalImage.rows);
		movedImage = imageWithBorder(roi);
	}
	else if (direction == 2) {
		copyMakeBorder(originalImage, imageWithBorder, 0, pixels, 0, 0, BORDER_REFLECT);
		Rect roi = Rect(0, pixels, originalImage.cols, originalImage.rows);
		movedImage = imageWithBorder(roi);
	}
	else if (direction == 3) {
		copyMakeBorder(originalImage, imageWithBorder, 0, 0, pixels, 0, BORDER_REFLECT);
		Rect roi = Rect(0, 0, originalImage.cols, originalImage.rows);
		movedImage = imageWithBorder(roi);
	}
	else if (direction == 4) {
		copyMakeBorder(originalImage, imageWithBorder, 0, 0, 0, pixels, BORDER_REFLECT);
		Rect roi = Rect(pixels, 0, originalImage.cols, originalImage.rows);
		movedImage = imageWithBorder(roi);
	}
	imageWithBorder.release();
}

void openOutputVideo3(VideoCapture &video, VideoWriter &videoWriter, String destinationVideo) {
	Size size = getVideoSize3(video);
	int fourcc = CV_FOURCC('X', '2', '6', '4');
	// Open the output
	videoWriter.open(destinationVideo, fourcc, video.get(CV_CAP_PROP_FPS), size, true);

	if (!videoWriter.isOpened())
	{
		cout << "Could not open the output video for write: " << destinationVideo << endl;
	}

	cout << "Input frame resolution: Width=" << size.width << "  Height=" << size.height
		<< " of nr#: " << video.get(CV_CAP_PROP_FRAME_COUNT) << endl;
}

void applyMotionBlur3(Mat &originalImage, Mat &bluredImage, double blurRadius, String direction)
{
	Mat polarCoordinatesImage, reflectiveBordersImage, bluredImagePolarCoordinates, croppedBlurredImagePolarCoordinates;
	if(direction == "up-down") {
		blur(originalImage, bluredImage, Size(1, blurRadius), Point(-1, -1), BORDER_REFLECT);
	}
	else if (direction == "left-right") {
		blur(originalImage, bluredImage, Size(blurRadius, 1), Point(-1, -1), BORDER_REFLECT);
	}
}

void applySwipeTransition(VideoCapture &video1, VideoCapture &video2, double transitionLength, String transitionDirection, point bezierPointB, point bezierPointC, point bezierPointB1, point bezierPointC1) {
	VideoWriter outputVideo;
	openOutputVideo3(video1, outputVideo, "Videos/a1.mp4");
	int firstVideoDirection, secondVideoDirection;
	String blurDirection = "left-right";
	if (transitionDirection == "down") {
		firstVideoDirection = 1;
		secondVideoDirection = 2;
		blurDirection = "up-down";
	}
	else if (transitionDirection == "up") {
		firstVideoDirection = 2;
		secondVideoDirection = 1;
		blurDirection = "up-down";
	}
	else if (transitionDirection == "left") {
		firstVideoDirection = 4;
		secondVideoDirection = 3;
		blurDirection = "left-right";
	}
	else if (transitionDirection == "right") {
		firstVideoDirection = 3;
		secondVideoDirection = 4;
		blurDirection = "left-right";
	}

	double frameCountVideo1 = video1.get(CAP_PROP_FRAME_COUNT);
	double numberOfFramesFromFirstVideo = (transitionLength / 2) * video1.get(CV_CAP_PROP_FPS);
	double indexOfFirstTransitionFrameInFirstVideo = frameCountVideo1 - numberOfFramesFromFirstVideo - 3;

	//bezier points
	point a = { 0, 0 };
	point d = { 1, 1 };

	int i = 0;
	for (;;) //hack to skip frames
	{
		video1.grab();
		i++;
		if (indexOfFirstTransitionFrameInFirstVideo < i) break;

	}

	Mat frame, intermidiateFrame, resultingFrame;
	
	float bezierResult, conversionMovementResult, conversionBlurResultRadius;
	for (int i = 0; i <= numberOfFramesFromFirstVideo; i++) {
		video1 >> frame;
		bezierResult = bezier(a, bezierPointB, bezierPointC, d, i / numberOfFramesFromFirstVideo);
		conversionMovementResult = computeInRange3(0, 50, bezierResult);
		conversionBlurResultRadius = computeInRange3(10, 500, bezierResult);
		moveImage(frame, intermidiateFrame, conversionMovementResult, firstVideoDirection);
		applyMotionBlur3(intermidiateFrame, resultingFrame, conversionBlurResultRadius, blurDirection);
		outputVideo << resultingFrame;
	}

	point a2 = { 0, 0 };
	point d2 = { 1, 1 };

	float numberOfFramesInSecondVideo = (transitionLength / 2) * video2.get(CV_CAP_PROP_FPS);
	for (int i = 0; i < numberOfFramesInSecondVideo; i++) {
		video2 >> frame;
		bezierResult = bezier(a2, bezierPointB1, bezierPointC1, d2, i / numberOfFramesInSecondVideo);
		conversionMovementResult = computeInRange3(50, 1, bezierResult);
		conversionBlurResultRadius = computeInRange3(500, 10, bezierResult);
		moveImage(frame, intermidiateFrame, conversionMovementResult, secondVideoDirection);
		applyMotionBlur3(intermidiateFrame, resultingFrame, conversionBlurResultRadius, blurDirection);
		outputVideo << resultingFrame;
	}

	outputVideo.release();
	video1.release();
	video2.release();
}
#pragma once

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write
#include "Bezier.h"

void applyTimeRemapping(VideoCapture &video1, double speedFactor);
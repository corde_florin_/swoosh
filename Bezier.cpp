#include "stdafx.h"
#include "Bezier.h"



// simple linear interpolation between two points
void lerp(point& dest, const point& a, const point& b, const float t)
{
	dest.x = a.x + (b.x - a.x)*t;
	dest.y = a.y + (b.y - a.y)*t;
}

// evaluate a point on a bezier-curve. t goes from 0 to 1.0
float bezier(const point& a, const point& b, const point& c, const point& d, const double t)
{
	point ab, bc, cd, abbc, bccd, dest;
	lerp(ab, a, b, t);           // point between a and b (green)
	lerp(bc, b, c, t);           // point between b and c (green)
	lerp(cd, c, d, t);           // point between c and d (green)
	lerp(abbc, ab, bc, t);       // point between ab and bc (blue)
	lerp(bccd, bc, cd, t);       // point between bc and cd (blue)
	lerp(dest, abbc, bccd, t);   // point on the bezier-curve (black)
	return dest.y;
}
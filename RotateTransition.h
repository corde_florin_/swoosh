#pragma once

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write

void applyRotateRightTransition(VideoCapture &video1, VideoCapture &video2, double firstClipLength, double secondClipLength);
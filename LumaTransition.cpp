#include "stdafx.h"
#include "common.h"
#include <iostream>
#include <string> 
#include <math.h>
#include "LumaTransition.h"
#include "Bezier.h"
using namespace std;
using namespace cv;

float computeInRange2(float rangeLeftLimit, float rangeRightLimit, double valueToConvert) {
	//value to convert is in the 0 - 1 range
	return valueToConvert * (rangeRightLimit - rangeLeftLimit) + rangeLeftLimit;
}

Size getVideoSize2(VideoCapture &video) {
	return Size((int)video.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)video.get(CV_CAP_PROP_FRAME_HEIGHT));
}

void openOutputVideo2(VideoCapture &video, VideoWriter &videoWriter, String destinationVideo) {
	Size size = getVideoSize2(video);
	int fourcc = CV_FOURCC('X', '2', '6', '4');
	// Open the output
	videoWriter.open(destinationVideo, fourcc, video.get(CV_CAP_PROP_FPS), size, true);

	if (!videoWriter.isOpened())
	{
		cout << "Could not open the output video for write: " << destinationVideo << endl;
	}

	cout << "Input frame resolution: Width=" << size.width << "  Height=" << size.height
		<< " of nr#: " << video.get(CV_CAP_PROP_FRAME_COUNT) << endl;
}

void applyLumaEffect(Mat &video1, Mat &video2, Mat &resultingFrame, double brightnessTreshold) {
	resultingFrame = Mat::zeros(video1.size(), video1.type());
	unsigned char *outputImageIterator = (unsigned char*)(resultingFrame.data);
	unsigned char *video1Iterator = (unsigned char*)(video1.data);
	unsigned char *video2Iterator = (unsigned char*)(video2.data);
	int pixelBrigthnessVideo1 = 0;
	int pixelBrigthnessVideo2 = 0;
	for (int i = 0; i < video1.cols * 3; i = i + 3) {
		for (int j = 0; j < video1.rows; j++) {
			pixelBrigthnessVideo1 = (video1Iterator[video1.step * j + i] + video1Iterator[video1.step * j + i + 1] + video1Iterator[video1.step * j + i + 2]) / 3;
			if (pixelBrigthnessVideo1 > brightnessTreshold) {
				outputImageIterator[resultingFrame.step * j + i] = video2Iterator[video2.step * j + i];
				outputImageIterator[resultingFrame.step * j + i + 1] = video2Iterator[video2.step * j + i + 1];
				outputImageIterator[resultingFrame.step * j + i + 2] = video2Iterator[video2.step * j + i + 2];
				
			}
			else {
				outputImageIterator[resultingFrame.step * j + i] = video1Iterator[video1.step * j + i];
				outputImageIterator[resultingFrame.step * j + i + 1] = video1Iterator[video1.step * j + i + 1];
				outputImageIterator[resultingFrame.step * j + i + 2] = video1Iterator[video1.step * j + i + 2];
			}
		}
	}

}

void applyLumaTransition(VideoCapture &video1, VideoCapture &video2, double transitionLength) {
	VideoWriter outputVideo;
	openOutputVideo2(video1, outputVideo, "Videos/luma.mp4");

	double frameCountVideo1 = video1.get(CAP_PROP_FRAME_COUNT);
	double numberOfTransitionFrames = transitionLength * video1.get(CV_CAP_PROP_FPS);
	double indexOfFirstTransitionFrameInFirstVideo = frameCountVideo1 - numberOfTransitionFrames - 3;

	//bezier points
	point a = { 0, 0 };
	point b = { 0, 1.75 };
	point c = { 1, -0.92 };
	point d = { 1, 1 };

	int i = 0;
	for (;;) //hack to skip frames
	{
		video1.grab();
		i++;
		if (indexOfFirstTransitionFrameInFirstVideo < i) break;

	}
	

	Mat frameVideo1, frameVideo2, resultingFrame;
	
	float bezierResult, conversionRotationResult, conversionBlurResult, conversionBlurResultRadius;
	for (int i = 0; i <= numberOfTransitionFrames; i++) {
		video1 >> frameVideo1;
		video2 >> frameVideo2;
		bezierResult = bezier(a, b, c, d, i / numberOfTransitionFrames);
		conversionRotationResult = computeInRange2(255, 0, bezierResult);
		applyLumaEffect(frameVideo1, frameVideo2, resultingFrame, conversionRotationResult);
		bezierResult = bezier(a, b, c, d, i / numberOfTransitionFrames);
		outputVideo << resultingFrame;
	}

	outputVideo.release();
	video1.release();
	video2.release();
}
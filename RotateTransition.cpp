#include "stdafx.h"
#include "common.h"
#include <iostream>
#include <string> 
#include <math.h>
#include "RotateTransition.h"
#include "Bezier.h"
using namespace std;
using namespace cv;

float computeInRange1(float rangeLeftLimit, float rangeRightLimit, double valueToConvert) {
	//value to convert is in the 0 - 1 range
	return valueToConvert * (rangeRightLimit - rangeLeftLimit) + rangeLeftLimit;
}

Size getVideoSize1(VideoCapture &video) {
	return Size((int)video.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)video.get(CV_CAP_PROP_FRAME_HEIGHT));
}

void rotateRight1(Mat &originalImage, Mat &rotatedImage, double degrees)
{
	Mat matRotation = getRotationMatrix2D(Point(originalImage.cols / 2, originalImage.rows / 2), degrees, 1);
	// Rotate the image
	warpAffine(originalImage, rotatedImage, matRotation, originalImage.size(), INTER_LINEAR, BORDER_REFLECT, Scalar());
}

void openOutputVideo1(VideoCapture &video, VideoWriter &videoWriter, String destinationVideo) {
	Size size = getVideoSize1(video);
	int fourcc = CV_FOURCC('X', '2', '6', '4');
	// Open the output
	videoWriter.open(destinationVideo, fourcc, video.get(CV_CAP_PROP_FPS), size, true);

	if (!videoWriter.isOpened())
	{
		cout << "Could not open the output video for write: " << destinationVideo << endl;
	}

	cout << "Input frame resolution: Width=" << size.width << "  Height=" << size.height
		<< " of nr#: " << video.get(CV_CAP_PROP_FRAME_COUNT) << endl;
}

void blurManualy(Mat &inputImage, Mat &outputImage, int kernelSize) {
	//input matrix pointer
	unsigned char *input = (unsigned char*)(inputImage.data);
	//set up output
	outputImage = Mat::zeros(inputImage.size(), inputImage.type());
	unsigned char *output = (unsigned char*)(outputImage.data);

	int i, j, k, invalidPixelsNo = 0, currentPixelRow, curentPixelBlue, curentPixelGreen, curentPixelRed;
	int halfKernelSize, bluredPixelValueBlue, bluredPixelValueGreen, bluredPixelValueRed;
	halfKernelSize = kernelSize / 2;

	for (i = 0; i < inputImage.cols * 3; i = i + 3) {
		for (j = 0; j < inputImage.rows; j++) {
			for (k = 0; k < kernelSize; k++) {
				currentPixelRow = kernelSize / 2 - k + j;
				curentPixelBlue = inputImage.step * currentPixelRow + i;
				curentPixelGreen = curentPixelBlue + 1;
				curentPixelRed = curentPixelGreen + 1;

				if (currentPixelRow < 0 || currentPixelRow >= inputImage.rows || (input[curentPixelBlue] == 0 && input[curentPixelGreen] == 0 && input[curentPixelRed] == 0)) {
					invalidPixelsNo++;
				}
				else {
					bluredPixelValueBlue = bluredPixelValueBlue + input[curentPixelBlue];
					bluredPixelValueGreen = bluredPixelValueGreen + input[curentPixelGreen];
					bluredPixelValueRed = bluredPixelValueRed + input[curentPixelRed];
				}
			}
			if (invalidPixelsNo < kernelSize) {

				bluredPixelValueBlue = bluredPixelValueBlue / (kernelSize - invalidPixelsNo);
				bluredPixelValueGreen = bluredPixelValueGreen / (kernelSize - invalidPixelsNo);
				bluredPixelValueRed = bluredPixelValueRed / (kernelSize - invalidPixelsNo);
				output[outputImage.step * j + i] = bluredPixelValueBlue;
				output[outputImage.step * j + i + 1] = bluredPixelValueGreen;
				output[outputImage.step * j + i + 2] = bluredPixelValueRed;
				bluredPixelValueBlue = 0;
				bluredPixelValueGreen = 0;
				bluredPixelValueRed = 0;
			}
			invalidPixelsNo = 0;
		}
	}
}

void applyMotionBlur1(Mat &originalImage, Mat &bluredImage, double blurRadius)
{
	Mat polarCoordinatesImage, reflectiveBordersImage, bluredImagePolarCoordinates, croppedBlurredImagePolarCoordinates;
	//compute the center of the original image
	Point2f center((float)originalImage.cols / 2, (float)originalImage.rows / 2);
	//transform image to polar coordinates
	linearPolar(originalImage, polarCoordinatesImage, center, originalImage.cols + originalImage.rows, CV_INTER_LINEAR + CV_WARP_FILL_OUTLIERS);
	//in order to blur the top and bottom of the image we apply this hack
	copyMakeBorder(polarCoordinatesImage, reflectiveBordersImage, 100, 100, 0, 0, BORDER_WRAP);
	blurManualy(reflectiveBordersImage, bluredImagePolarCoordinates, blurRadius);
	Rect roi = Rect(0, 100, originalImage.cols, originalImage.rows);
	croppedBlurredImagePolarCoordinates = bluredImagePolarCoordinates(roi);

	//we apply the inverse polar transformation
	linearPolar(croppedBlurredImagePolarCoordinates, bluredImage, center, originalImage.cols + originalImage.rows, CV_INTER_LINEAR + WARP_INVERSE_MAP);
}


void applyRotateRightTransition(VideoCapture &video1, VideoCapture &video2, double firstClipLength, double secondClipLength) {
	VideoWriter outputVideo;
	openOutputVideo1(video1, outputVideo, "Videos/state1.mp4");

	double frameCountVideo1 = video1.get(CAP_PROP_FRAME_COUNT);
	double numberOfFramesFromFirstVideo = firstClipLength * video1.get(CV_CAP_PROP_FPS);
	double indexOfFirstTransitionFrameInFirstVideo = frameCountVideo1 - numberOfFramesFromFirstVideo - 3;

	//bezier points
	point a = { 0, 0 };
	point b = { 1, 0 };
	point c = { 1, 0.1 };
	point d = { 1, 1 };

	int i = 0;
	for (;;) //hack to skip frames
	{
		video1.grab();
		i++;
		if (indexOfFirstTransitionFrameInFirstVideo - 20 < i) break;

	}

	Mat frame, resultingFrame;

	float bezierResult, conversionRotationResult, conversionBlurResult, conversionBlurResultRadius;
	for (int i = 0; i <= numberOfFramesFromFirstVideo; i++) {
		video1 >> frame;
		bezierResult = bezier(a, b, c, d, i / numberOfFramesFromFirstVideo);
		conversionRotationResult = computeInRange1(0, 90, bezierResult);
		conversionBlurResult = computeInRange1(1, 50, bezierResult);
		conversionBlurResultRadius = computeInRange1(10, 90, bezierResult);
		rotateRight1(frame, frame, conversionRotationResult);
		applyMotionBlur1(frame, resultingFrame, conversionBlurResultRadius);
		outputVideo << resultingFrame;
	}

	point a2 = { 0, 0 };
	point b2 = { 0, 1 };
	point c2 = { 0, 1 };
	point d2 = { 1, 1 };

	float numberOfFramesInSecondVideo = secondClipLength * video2.get(CV_CAP_PROP_FPS);
	for (int i = 0; i < numberOfFramesInSecondVideo; i++) {
		video2 >> frame;
		bezierResult = bezier(a2, b2, c2, d2, i / numberOfFramesInSecondVideo);
		conversionRotationResult = computeInRange1(-90, 0, bezierResult);
		conversionBlurResult = computeInRange1(50, 1, bezierResult);
		conversionBlurResultRadius = computeInRange1(90, 10, bezierResult);
		rotateRight1(frame, frame, conversionRotationResult);
		applyMotionBlur1(frame, resultingFrame, conversionBlurResultRadius);
		outputVideo << resultingFrame;
	}

	outputVideo.release();
	video1.release();
	video2.release();
}
#pragma once

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write
#include "Bezier.h"

void applySwipeTransition(VideoCapture &video1, VideoCapture &video2, double transitionLength, String transitionDirection, point bezierPointB, point bezierPointC, point bezierPointB1, point bezierPointC1);
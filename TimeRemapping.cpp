#include "stdafx.h"
#include "common.h"
#include <iostream>
#include <string> 
#include <math.h>
#include "TimeRemapping.h"
using namespace std;
using namespace cv;


float computeInRange4(float rangeLeftLimit, float rangeRightLimit, double valueToConvert) {
	//value to convert is in the 0 - 1 range
	return valueToConvert * (rangeRightLimit - rangeLeftLimit) + rangeLeftLimit;
}

Size getVideoSize4(VideoCapture &video) {
	return Size((int)video.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)video.get(CV_CAP_PROP_FRAME_HEIGHT));
}

void openOutputVideo4(VideoCapture &video, VideoWriter &videoWriter, String destinationVideo) {
	Size size = getVideoSize4(video);
	int fourcc = CV_FOURCC('X', '2', '6', '4');
	// Open the output
	videoWriter.open(destinationVideo, fourcc, 25, size, true);

	if (!videoWriter.isOpened())
	{
		cout << "Could not open the output video for write: " << destinationVideo << endl;
	}

	cout << "Input frame resolution: Width=" << size.width << "  Height=" << size.height
		<< " of nr#: " << video.get(CV_CAP_PROP_FRAME_COUNT) << endl;
}

void applyTimeRemapping(VideoCapture &video, double speedFactor) {
	VideoWriter outputVideo;
	openOutputVideo4(video, outputVideo, "Videos/SlowMotion.mp4");
	double frameSkipper = speedFactor * video.get(CV_CAP_PROP_FPS) / 25;
	double frameCountVideo = video.get(CAP_PROP_FRAME_COUNT);
	Mat frame;
	double numberOfFramesToBeSkiped = (long) frameSkipper;
	double fractionPart = frameSkipper - numberOfFramesToBeSkiped;


	if (frameSkipper >= 1) {
		for (int i = 0; i < frameCountVideo; i++) {
			video >> frame;
			outputVideo << frame;
			
			for (int j = 1; j < numberOfFramesToBeSkiped + fractionPart; j++) {
				video.grab();
				i++;
			}
			if (fractionPart > 1)
				fractionPart = fractionPart - 1;
			fractionPart = fractionPart + fractionPart;
		}
	}
	else {
		cout << "Too slow! Not enough frames for a smooth video!!";
	}

}
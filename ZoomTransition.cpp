#include "stdafx.h"
#include "common.h"
#include <iostream>
#include <string> 
#include <math.h>
#include "ZoomTransition.h"
#include "Bezier.h"

using namespace std;
using namespace cv;

float computeInRange(float rangeLeftLimit, float rangeRightLimit, double valueToConvert) {
	//value to convert is in the 0 - 1 range
	return valueToConvert * (rangeRightLimit - rangeLeftLimit) + rangeLeftLimit;
}

Size getVideoSize(VideoCapture &video) {
	return Size((int)video.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)video.get(CV_CAP_PROP_FRAME_HEIGHT));
}

void openOutputVideo(VideoCapture &video, VideoWriter &videoWriter, String destinationVideo) {
	Size size = getVideoSize(video);
	int fourcc = CV_FOURCC('X', '2', '6', '4');
	// Open the output
	videoWriter.open(destinationVideo, fourcc, video.get(CV_CAP_PROP_FPS), size, true);

	if (!videoWriter.isOpened())
	{
		cout << "Could not open the output video for write: " << destinationVideo << endl;
	}

	cout << "Input frame resolution: Width=" << size.width << "  Height=" << size.height
		<< " of nr#: " << video.get(CV_CAP_PROP_FRAME_COUNT) << endl;
}

Mat zoomIn(Mat &originalImage, double zoom)
{
	int interiorSquareWidth = originalImage.cols * (1 / zoom);
	int interiorSquarHeight = originalImage.rows * (1 / zoom);
	int x = (originalImage.cols - interiorSquareWidth) / 2;
	int y = (originalImage.rows - interiorSquarHeight) / 2;
	Rect roi = Rect(x, y, interiorSquareWidth, interiorSquarHeight);
	Mat resultingImage = originalImage(roi);
	resize(resultingImage, resultingImage, Size(originalImage.cols, originalImage.rows), 0, 0, CV_INTER_AREA);

	return resultingImage;
}

Mat zoomOut(Mat &originalImage, double zoom)
{
	int scaledImageCols = originalImage.cols * zoom;
	int scaledImageRows = originalImage.rows * zoom;
	//division problems
	if (scaledImageRows % 2 == 1)
		scaledImageRows++;
	if (scaledImageCols % 2 == 1)
		scaledImageCols++;

	int padPixelsTopAndBotom = (originalImage.rows - scaledImageRows) / 2;
	int padPixelsLeftAndright = (originalImage.cols - scaledImageCols) / 2;
	Mat resultingImage;
	resize(originalImage, resultingImage, Size(scaledImageCols, scaledImageRows), 0, 0, CV_INTER_AREA);
	copyMakeBorder(resultingImage, resultingImage, padPixelsTopAndBotom, padPixelsTopAndBotom, padPixelsLeftAndright, padPixelsLeftAndright, BORDER_REFLECT);
	return resultingImage;
}

Mat applyMotionBlur(Mat &originalImage, double blurRadius, int iterations)
{
	float center_x = originalImage.cols / 2;
	float center_y = originalImage.rows / 2;
	float blur = blurRadius;

	Mat growMapx(originalImage.size(), CV_32FC1);
	Mat growMapy(originalImage.size(), CV_32FC1);
	Mat shrinkMapx(originalImage.size(), CV_32FC1);
	Mat shrinkMapy(originalImage.size(), CV_32FC1);

	for (int x = 1; x < originalImage.cols; x++) {
		for (int y = 1; y < originalImage.rows; y++) {
			growMapx.at<float>(y, x) = x + ((x - center_x)*blur);
			if (growMapx.at<float>(y, x) < 1)
				growMapx.at<float>(y, x) = 1;
			growMapy.at<float>(y, x) = y + ((y - center_y)*blur);
			if (growMapy.at<float>(y, x) < 1)
				growMapy.at<float>(y, x) = 1;
			shrinkMapx.at<float>(y, x) = x - ((x - center_x)*blur);
			if (shrinkMapx.at<float>(y, x) < 1)
				shrinkMapx.at<float>(y, x) = 1;
			shrinkMapy.at<float>(y, x) = y - ((y - center_y)*blur);
			if (shrinkMapy.at<float>(y, x) < 1)
				shrinkMapy.at<float>(y, x) = 1;
		}
	}

	Mat tmp1, tmp2;
	for (int i = 0; i < iterations; i++) {
		remap(originalImage, tmp1, growMapx, growMapy, CV_INTER_LINEAR, BORDER_REFLECT); // enlarge
		remap(originalImage, tmp2, shrinkMapx, shrinkMapy, CV_INTER_LINEAR, BORDER_REFLECT); // shrink
		addWeighted(tmp1, 0.5, tmp2, 0.5, 0, originalImage); // blend back to src
	}

	return originalImage; 
}

void applyZoomInTransition(VideoCapture &video1, VideoCapture &video2, double firstClipLength, double secondClipLength)
{
	VideoWriter outputVideo;
	openOutputVideo(video1, outputVideo, "Videos/destinationvideo7.mp4");
	
	double frameCountVideo1 = video1.get(CAP_PROP_FRAME_COUNT);
	double numberOfFramesFromFirstVideo = firstClipLength * video1.get(CV_CAP_PROP_FPS);
	double indexOfFirstTransitionFrameInFirstVideo = frameCountVideo1 - numberOfFramesFromFirstVideo - 3;

	//bezier points
	point a = { 0, 0 };
	point b = { 1, 0 };
	point c = { 1, 0.1 };
	point d = { 1, 1 };

	int i = 0;
	for (;;) //hack to skip frames
	{
		video1.grab();
		i++;
		if (indexOfFirstTransitionFrameInFirstVideo < i) break;

	}

	//cut and zoom first clip
	Mat frame;
	float bezierResult, conversionZoomResult, conversionBlurResult, conversionBlurResultRadius;
	for (int i = 0; i <= numberOfFramesFromFirstVideo; i++) {
		video1 >> frame;
		bezierResult = bezier(a, b, c, d, i / numberOfFramesFromFirstVideo);
		conversionZoomResult = computeInRange(1, 3, bezierResult);
		conversionBlurResult = computeInRange(1, 50, bezierResult);
		conversionBlurResultRadius = computeInRange(0.3, 0, bezierResult);
		frame = zoomIn(frame, conversionZoomResult);
		outputVideo << applyMotionBlur(frame, conversionBlurResultRadius*conversionBlurResultRadius, conversionBlurResult);
	}

	point a2 = { 0, 0 };
	point b2 = { 0, 1 };
	point c2 = { 0, 1 };
	point d2 = { 1, 1 };

	float numberOfFramesInSecondVideo = secondClipLength * video2.get(CV_CAP_PROP_FPS);
	for (int i = 0; i < numberOfFramesInSecondVideo; i++) {
		video2 >> frame;
		bezierResult = bezier(a2, b2, c2, d2, i / numberOfFramesInSecondVideo);
		conversionZoomResult = computeInRange(0.64, 1, bezierResult);
		conversionBlurResult = computeInRange(50, 1, bezierResult);
		conversionBlurResultRadius = computeInRange(0, 0.3, bezierResult);
		frame = zoomOut(frame, conversionZoomResult);
		outputVideo << applyMotionBlur(frame, conversionBlurResultRadius*conversionBlurResultRadius, conversionBlurResult);
	}

	outputVideo.release();
	video1.release();
	video2.release();
}

void applyZoomOutTransition(VideoCapture &video1, VideoCapture &video2, double firstClipLength, double secondClipLength) {
	VideoWriter outputVideo;
	openOutputVideo(video1, outputVideo, "Videos/last221.mp4");

	double frameCountVideo1 = video1.get(CAP_PROP_FRAME_COUNT);
	double numberOfFramesFromFirstVideo = firstClipLength * video1.get(CV_CAP_PROP_FPS);
	double indexOfFirstTransitionFrameInFirstVideo = frameCountVideo1 -numberOfFramesFromFirstVideo - 3;

	//bezier points
	point a = { 0, 0 };
	point b = { 1, 0 };
	point c = { 1, 0.1 };
	point d = { 1, 1 };

	int i = 0;
	for (;;) //hack to skip frames
	{
		video1.grab();
		i++;
		if (indexOfFirstTransitionFrameInFirstVideo < i) break;
		
	}
	//cut and zoom first clip
	Mat frame;
	float bezierResult, conversionZoomResult, conversionBlurResult, conversionBlurResultRadius;
	for (int i = 0; i < numberOfFramesFromFirstVideo; i++) {
		video1 >> frame;
		bezierResult = bezier(a, b, c, d, i / numberOfFramesFromFirstVideo);
		conversionZoomResult = computeInRange(1, 0.4, bezierResult);
		conversionBlurResult = computeInRange(1, 50, bezierResult);
		conversionBlurResultRadius = computeInRange(0, 0.15, bezierResult);
		frame = zoomOut(frame, conversionZoomResult);
		outputVideo << applyMotionBlur(frame, conversionBlurResultRadius*conversionBlurResultRadius, conversionBlurResult);
	}

	point a2 = { 0, 0 };
	point b2 = { 0, 1 };
	point c2 = { 0, 1 };
	point d2 = { 1, 1 };

	//cut and zoom second clip
	float numberOfFramesInSecondVideo = secondClipLength * video1.get(CV_CAP_PROP_FPS);
	for (int i = 0; i <= numberOfFramesInSecondVideo; i++) {
		video2 >> frame;
		bezierResult = bezier(a2, b2, c2, d2, i / numberOfFramesInSecondVideo);
		conversionZoomResult = computeInRange(1.4, 1, bezierResult);
		conversionBlurResult = computeInRange(50, 1, bezierResult);
		conversionBlurResultRadius = computeInRange(0.1, 0, bezierResult);
		frame = zoomIn(frame, conversionZoomResult);
		outputVideo << applyMotionBlur(frame, conversionBlurResultRadius*conversionBlurResultRadius, conversionBlurResult);
	}

	outputVideo.release();
	video1.release();
	video2.release();
}